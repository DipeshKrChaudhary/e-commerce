import React from "react";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ExposureIcon from '@material-ui/icons/Exposure';
import HighlightsData from "./HighlightsData";
function MainContent() {
    return(
        <>
         <a href="#">
                <h1 className="heightLight">Highlight</h1>
                <div className="highlights">
                    <figure className="card">
                    <img src={props.imgsrc} alt="Img." />
                    <button className="likeBtn">
                        <FavoriteBorderIcon />
                        4k
                    </button>
                    <figcaption className="caption">
                        <h3 className="caption-title">{props.title}</h3>
                        <p>
                        {props.desc}
                        </p>
                    </figcaption>
                    </figure>
                </div>
                </a>
                <nav id="single-na" className="single-na menu" role="navigation">
                <ul>
                    <li>
                    <a className="default">
                        Nrs: <b>{props.price}</b>
                    </a>
                    </li>
                    <li className="defs">
                    <button>
                        <ExposureIcon />
                    </button>{" "}
                    Bargin
                    </li>
                    <li className="defs">
                    <button>
                        <AddBoxIcon />
                    </button>{" "}
                    Add To Cart
                    </li>
                </ul>
                </nav>
        </>

    );
}

export default MainContent;