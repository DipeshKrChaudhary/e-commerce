import React from "react";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ExposureIcon from '@material-ui/icons/Exposure';
import JustForYouData from "./JustForYouData";

 function JustGrid (props) {
    return(
        <div className="jfy">
        <li className="card-item  semi">
          <a href="#">
            <figure className="card">
              <img src={props.imgsrc} alt="Img." />
              <figcaption className="caption">
                <h3 className="caption-title">{props.title}</h3>
                <p>{props.desc}</p>
              </figcaption>
            </figure>
          </a>
          <nav id="single-na" className="single-na menu" role="navigation">
            <ul>
              <li>
                <a className="default">
                  Nrs: <b>{props.price}</b>
                </a>
              </li>
              <div className="buttomIcon">
                <li className="defs">
                  <button>
                    <ExposureIcon />
                  </button>
                </li>
                <li className="defs">
                  <button>
                    <AddBoxIcon />
                  </button>
                </li>
                <li className="defs">
                  <button>
                    <FavoriteBorderIcon />
                    4k
                  </button>
                </li>
              </div>
            </ul>
          </nav>
        </li>
      </div>
    );
}

function JustForYou(){
    return(
        <>
        <div className="justForYou">
  <header className="masthead">
    <h1 className="site-title">Just for you</h1>
  </header>
  <div className="jfybox">
    <ul className="cards b">

    {JustForYouData.map((val,index)=>
    {
        return(
        <JustGrid
            key={val.id}
            imgsrc={val.imgsrc}
            title={val.title}
            desc={val.desc}
            price={val.price}
            />
    );
    })}
    </ul>
  </div>
</div>

        </>
    );
}

export  default JustForYou ;