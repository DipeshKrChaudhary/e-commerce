import React from "react";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ExposureIcon from '@material-ui/icons/Exposure';
import {Highlights} from './Highlights';
import StaffChoicesData from "./StaffChoicesData";

function StaffGrid(props){
  return(
    <div className="cardd">
    <li className="card-item">
      <a href="#">
        <figure className="card">
          <img src={props.imgsrc} alt="Img." />
          <figcaption className="caption">
            <h3 className="caption-title">{props.title}</h3>

            <p>{props.desc}</p>
          </figcaption>
        </figure>
      </a>
      <nav id="single-na" className="single-na menu" role="navigation">
        <ul>
          <li>
            <a className="default">
              Nrs: <b>{props.price}</b>
            </a>
          </li>
          <div className="buttomIcon">
            <li className="defs">
              <button>
                <ExposureIcon />
              </button>
            </li>
            <li className="defs">
              <button>
                <AddBoxIcon />
              </button>
            </li>
            <li className="defs">
              <button>
                <FavoriteBorderIcon />
                4k
              </button>
            </li>
          </div>
        </ul>
      </nav>
    </li>
  </div>
  );
}

function StaffChoices(){
    return(
        <>
        <ul className="cards">
          <Highlights />
          <div className="staffPick">
            <h1 className="heightLight">Staff Pick</h1>
            
            {StaffChoicesData.map((val,index)=>
            {
                return(
                <StaffGrid
                    key={val.id}
                    imgsrc={val.imgsrc}
                    title={val.title}
                    desc={val.desc}
                    price={val.price}
                    />
                     );
            })}
          </div>
        </ul>
        </>
    );
}

export default StaffChoices;