import React from "react";
import SingleNav from "./SingleNav";
import ImageAvatars from "./Avatar";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Modal from "./Modal";
import OutlinedCard from "./PopUp";
function AdvanceNav(){ 
    const modalRef=React.useRef();
    const openModal=()=>{
        modalRef.current.openModal()
    };
    return(
        <>
        <div className="topBar">
         <SingleNav/>       
            <section className="menu-section2">
                <nav id="advanced-nav" className="advanced-nav menu" role="navigation">
                    <ul className="adv">
                        <li>
                           <a href="#"> 
                                <div className="logo">
                                   <img src="Logo.png"/>
                                </div>                               
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            <div className="searchBox">
                           <input  className="sBox" type="text" placeholder="Enter keywords for your search" />
                           <button className="Sbtn" type="submit"><img  className="Simg" src="Vector.png"/></button>
                           </div>
                            </a>
                        </li>
      
                        <li>
                            <div className="ava">
                            <a href="#">
                                <div className="AvatarIcon">
                                    <ImageAvatars/>
                                </div>
                                <div className="AvatarDrop">
                                      Neeta <br/>
                                    Teemsina
                                </div>
                                <button className="dropDown"><ArrowDropDownIcon/></button>
                            </a>
                            </div>
                        </li>

                        <li>
                            <div className="ava">
                            <a href="#">
                                <div className="AvatarFav ">
                               <button onClick={openModal} className="btnCss"> <FavoriteBorderIcon/></button>
                               <Modal ref={modalRef}>
                                   <h1>Your Favorites</h1>
                                   <OutlinedCard/>
                                   <button className="popUpbtn" onClick={()=> modalRef.current.close()}>
                                   <HighlightOffIcon/>
                                   </button>
                               </Modal>
                                </div>
                                <div className="AvatarDrop btnCss">
                                     Favorites
                                </div>
                            </a>
                            </div>
                        </li>
                        <li>
                            <div className="ava">
                            <a href="#">
                                <div className="AvatarFav">
                                    <button className="btnCss" ><AddShoppingCartIcon/></button>
                                </div>
                                <div className="AvatarDrop btnCss">
                                     Cart
                                </div>
                            </a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </section>
            </div>

        </>
    );
}

export default AdvanceNav;