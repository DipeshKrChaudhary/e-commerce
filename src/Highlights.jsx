import React, { useState, useEffect } from "react";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ExposureIcon from '@material-ui/icons/Exposure';
import HighlightsData from "./HighlightsData";



 export function HighlightsGrid(props){
     const [name, setTitle] = useState(props.title);
    //  const [fullName, setFullName]=useState();
     
     const [image, setImage] = useState(props.imgsrc);
    //  const [fullImage, setFullImage]=useState();

     const [amount, setAmount] = useState(props.price);
    
    //  useEffect((e)=>{
    //      console.log('title trigged ');
    //      console.log(name);
    //  }, [])

  const  d=[name, image, amount];
     
    const onSubmit=()=>{
            // setFullName(name);
            // console.log(name);
            // setFullImage(image);
            // console.log(image);
            // setFullAmount(amount);
            // console.log(amount);
           const datas={image, name, amount}
           console.log(datas)           
      }
    
     return(
        <div className="carddd">
            <li className="card-item double">
                <a href="#">
                <h1 className="heightLight">Highlight </h1>
                
                <div className="highlights">
                    <figure className="card">
                    <img src={props.imgsrc} alt="img." value={image} onChange={e=>setImage(e.target.value )}/>
                    <button  onClick={onSubmit}  className="likeBtn">
                        <FavoriteBorderIcon />
                        4k
                    </button>
                    <figcaption className="caption">
                        <h3 className="caption-title" value={name} onChange={e=>setTitle(e.target.value)}>{props.title}</h3>
                        <p>
                        {props.desc}
                        </p>
                    </figcaption>
                    </figure>
                </div>
                </a>
                <nav id="single-na" className="single-na menu" role="navigation">
                <ul>
                    <li>
                    <a value={amount} onChange={e=>setAmount(e.target.value )} className="default">
                        Nrs: <b>{props.price}</b>
                    </a>
                    </li>
                    <li className="defs">
                    <button>
                        <ExposureIcon />
                    </button>
                    Bargin
                    </li>
                    <li className="defs">
                    <button >
                        <AddBoxIcon />
                    </button>
                    Add To Cart
                    </li>
                </ul>
                </nav>
            </li>
            </div>
     )
 }
function Highlights(){     
    return(    
        <>
          
         {HighlightsData.map((val,index)=>
            {
                return(
                <HighlightsGrid
                    key={val.id}
                    imgsrc={val.imgsrc}
                    title={val.title}
                    desc={val.desc}
                    price={val.price}
                    />
                     );
            })}
           
            
        </>
    );
}

export {Highlights } ;