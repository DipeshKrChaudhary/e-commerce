import React from "react";
function SingleNav(){
    return(
        <>
             <section className="menu-section"> 
                <nav id="single-nav" className="single-nav menu" >
                    <ul>
                        <li><a href="#">Sell on Gahana</a></li>
                        <li><a href="#">Customer Care</a></li>
                        <li><a href="#">Track My Order</a></li>
                    </ul>
                </nav>
            </section>
        </>
    );
}

export default SingleNav;