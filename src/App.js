import React from 'react';
import StaffChoices from "./StaffChoices";
import JustForYou from "./JustForYou";
import AdvanceNav from './AdvanceNav';

const App =()=>{
  return(
    <> 
    <AdvanceNav/>            
    <StaffChoices/>
    <JustForYou/>
   
    </>
    )
}

export default App;