
import React from 'react';
import {Data,Highlights,HighlightsGrid } from './Highlights';

const list = [
 
];
const initialList = [
 
    {
      id:8,
      imgsrc:"Image 1.png",
      title:"Gahana",
      price:"400000"
    },
    {
        id:9,
        imgsrc:"Image 1.png",
        title:"Gahana",
        price:"400000"
      },
     
  ];
   
 
  export default function OutlinedCard(props)  {

    const [list, setList] = React.useState(initialList);
   
    function handleRemove(id) {
      const newList = list.filter((item) => item.id !== id);
   
      setList(newList);
    }
   
    return <List list={list} onRemove={handleRemove} />;
  };

  
  const List = ({ list, onRemove }) => (
    <ul>
      {list.map((item) => (
        <Item key={item.id} item={item} onRemove={onRemove} />
      ))}
    </ul>
  );
   
  const Item = ({ item, onRemove }) => (
    <li>
     <>        
              <div className="popUpRoot" >
               <div className="CardContent">
                   <img src={item.imgsrc} alt="img" style={{width: "100px"}}/>
                  
                   <ul>
                       <li><a className="favList" href="#">{item.title}</a></li>
                       <li><a className="favList" href="#">{item.price}</a></li>
                       <li><button className="btnRemove"type="button" onClick={() => onRemove(item.id)}>Remove</button></li>
                       </ul>
               </div>
               </div>
            </>   
        
    </li>
  );
